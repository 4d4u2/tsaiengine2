﻿using Emgu.CV;
using Emgu.CV.Structure;
using TsaiEngine.Models;
using System.Collections.Generic;
using System.Linq;

namespace TsaiEngine.Tsai
{
    /// <summary>
    /// Defines a focal finder class for finding F and Tz
    /// </summary>
    public class FocalFinder
    {
        #region Private Variables

        private Matrix<double> _a;
        private Matrix<double> _b;

        #endregion

        #region Main Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="featurePoints">The feature points that we are working with</param>
        /// <param name="centre">The centre of the image</param>
        /// <param name="rotation">The current rotation matrix</param>
        /// <param name="ty">The current value associated with ty</param>
        public FocalFinder(List<FeaturePoint> featurePoints, MCvPoint2D64f centre, Matrix<double> rotation, double ty) 
        {
            _a = InitializeMatrixA(featurePoints, centre, rotation, ty);
            _b = InitializeMatrixB(featurePoints, centre, rotation);
        }

        #endregion

        #region Getter Methods

        /// <summary>
        /// Retrieve the values associated with the A matrix
        /// </summary>
        /// <returns>The A matrix that we are working with</returns>
        public Matrix<double> GetAMatrix() 
        {
            return _a;
        }

        /// <summary>
        /// Retrieve the values associated with the B Matrix
        /// </summary>
        /// <returns>The B matrix that we are working with</returns>
        public Matrix<double> GetBMatrix() 
        {
            return _b;
        }

        #endregion

        #region Find the Solution

        /// <summary>
        /// Calculate the result of processing
        /// </summary>
        /// <returns>The result of processing the associated points</returns>
        public Matrix<double> CalculateResult() 
        {
            var result = new Matrix<double>(2, 1);
            CvInvoke.Solve(_a, _b, result, Emgu.CV.CvEnum.DecompMethod.QR);

            var test = _a * result;

            return result;
        }

        #endregion

        #region Initializer Helpers

        /// <summary>
        /// Defines a set of initializer helpers
        /// </summary>
        /// <param name="features">The list of feature points that we are dealing with</param>
        /// <param name="centre">The centre of the image</param>
        /// <param name="rotation">The rotation that we are using</param>
        /// <param name="ty">The current translation in the y direction</param>
        /// <returns>The resultant A matrix</returns>
        private Matrix<double> InitializeMatrixA(List<FeaturePoint> features, MCvPoint2D64f centre, Matrix<double> rotation, double ty)
        {
            var matrix = new Matrix<double>(features.Count, 2); 

            for (int i = 0; i < features.Count; i++) 
            {
                var X = features[i].GetSceneCoordinate().X;
                var Y = features[i].GetSceneCoordinate().Y;
                var Z = features[i].GetSceneCoordinate().Z;
                var r4 = rotation.Data[1, 0];
                var r5 = rotation.Data[1, 1];
                var r6 = rotation.Data[1, 2];
                
                var y = (features[i].GetImageCoordinate().Y - centre.Y);

                var value1 = r4 * X + r5 * Y + r6 * Z + ty;
                var value2 = -y;

                matrix[i, 0] = value1;
                matrix[i, 1] = value2;

            }
            return matrix;        
        }

        /// <summary>
        /// Defines a set of initializer helpers
        /// </summary>
        /// <param name="features">The list of feature points that we are dealing with</param>
        /// <param name="centre">The centre of the image</param>
        /// <param name="rotation">The rotation that we are using</param>
        /// <returns>The resultant B matrix</returns>
        private Matrix<double> InitializeMatrixB(List<FeaturePoint> features, MCvPoint2D64f centre, Matrix<double> rotation)
        {
            var matrix = new Matrix<double>(features.Count, 1);
            for (int i = 0; i < features.Count; i++)
            {
                var X = features[i].GetSceneCoordinate().X;
                var Y = features[i].GetSceneCoordinate().Y;
                var Z = features[i].GetSceneCoordinate().Z;
                var r7 = rotation.Data[2, 0];
                var r8 = rotation.Data[2, 1];
                var r9 = rotation.Data[2, 2];
                var y = (features[i].GetImageCoordinate().Y - centre.Y);

                var value = y * (r7 * X + r8 * Y + r9 * Z);
               
                matrix[i, 0] =  value;
 
            }
            return matrix;
        }

        #endregion
    }
}