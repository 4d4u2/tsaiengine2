﻿using Emgu.CV.Structure;
using LMDotNet;
using TsaiEngine.Models;
using System.Linq;
using System;
using ProjectionTest.Utilities;
using Emgu.CV;

namespace TsaiEngine.Tsai
{
    /// <summary>
    /// Estimate distortion based on the given linear model
    /// </summary>
    public class Refiner
    {
        #region Private Variables

        private CalibrationProblem _calibrationProblem;

        #endregion

        #region Entry Point into distortion estimation logic

        /// <summary>
        /// Defines the logic to optimize the associated calibration problem
        /// </summary>
        /// <param name="calibrationProblem">The calibration problem we are optimizing</param>
        public void OptimizeDistortion(CalibrationProblem calibrationProblem)
        {
            var solver = new LMSolver(epsilon:1e-09); 
            _calibrationProblem = calibrationProblem;
            double[] initialVector = GetInitialParameters(calibrationProblem);
            var pointCount = calibrationProblem.Features.Count;
            var result = solver.Minimize(GetScore, initialVector, pointCount);
            UpdateProblem(calibrationProblem, result.OptimizedParameters);
        }

        /// <summary>
        /// Retrieve the initial parameters for optimization
        /// </summary>
        /// <param name="calibrationProblem">The calibration problem that we are initializing from</param>
        /// <returns>The resultant vector of initial values</returns>
        private static double[] GetInitialParameters(CalibrationProblem calibrationProblem)
        {
            var focal = calibrationProblem.CalibrationResult.Focal;
            var tz = calibrationProblem.CalibrationResult.Translation[2, 0];
            var k1 = calibrationProblem.CalibrationResult.K1;
            var k2 = calibrationProblem.CalibrationResult.K2;
            var p1 = calibrationProblem.CalibrationResult.P1;
            var p2 = calibrationProblem.CalibrationResult.P2;
            var centreX = calibrationProblem.CalibrationResult.CentreOffset.X;
            var centreY = calibrationProblem.CalibrationResult.CentreOffset.Y;

            var rotationVector = new Matrix<double>(3, 1);
            CvInvoke.Rodrigues(calibrationProblem.CalibrationResult.Rotation, rotationVector);
            var rx = rotationVector.Data[0, 0];
            var ry = rotationVector.Data[1, 0];
            var rz = rotationVector.Data[2, 0];
            
            var ty = calibrationProblem.CalibrationResult.Translation.Data[1, 0];
            var tx = calibrationProblem.CalibrationResult.Translation.Data[0, 0];

            var initialVector = new double[] { focal, tz, k1, k2, p1, p2, centreX, centreY, rx, ry, rz, ty, tx };
            return initialVector;
        }

        #endregion

        #region Error Calculator

        /// <summary>
        /// Find the score for the associated parameters
        /// </summary>
        /// <param name="parameters">The parameters (f, tz, k1)</param>
        /// <param name="residuals">The associated residuals for the parameters</param>
        private void GetScore(double[] parameters, double[] residuals)
        {
            UpdateProblem(_calibrationProblem, parameters);
            var score = ErrorFinder.GetError(_calibrationProblem, residuals);
           // for (int i = 0; i < residuals.Length; i++) residuals[i] = score;
            //Console.WriteLine("Score: " + score);
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Update the calibration from a list of given parameters
        /// </summary>
        /// <param name="calibrationProblem">The calibration problem that we are updating</param>
        /// <param name="parameters">The parameters that we are updating from</param>
        private static void UpdateProblem(CalibrationProblem calibrationProblem, double[] parameters)
        {
            calibrationProblem.CalibrationResult.Focal = parameters[0];
            calibrationProblem.CalibrationResult.Translation[2, 0] = parameters[1];
            calibrationProblem.CalibrationResult.K1 = parameters[2];
            calibrationProblem.CalibrationResult.K2 = parameters[3];
            calibrationProblem.CalibrationResult.P1 = parameters[4];
            calibrationProblem.CalibrationResult.P2 = parameters[5];
            calibrationProblem.CalibrationResult.CentreOffset = new MCvPoint2D64f(parameters[6], parameters[7]);

            var rotationVector = new Matrix<double>(3, 1);
            rotationVector.Data[0, 0] = parameters[8];
            rotationVector.Data[1, 0] = parameters[9];
            rotationVector.Data[2, 0] = parameters[10];
            CvInvoke.Rodrigues(rotationVector, calibrationProblem.CalibrationResult.Rotation);

            calibrationProblem.CalibrationResult.Translation[1, 0] = parameters[11];
            calibrationProblem.CalibrationResult.Translation[0, 0] = parameters[12];
        }

        #endregion
    }
}
