﻿using Emgu.CV;
using Emgu.CV.Structure;
using TsaiEngine.Models;
using System;
using System.Collections.Generic;

namespace TsaiEngine.Tsai
{
    /// <summary>
    /// Extract values from the L matrix and other sources
    /// </summary>
    public class LExtractor
    {
        #region Main Entry Point

        /// <summary>
        /// Defines the logic to extract the result from calibration
        /// </summary>
        /// <param name="L">The associated L matrix</param>
        /// <param name="positiveTy">Whether to tread ty as positive or not</param>
        /// <param name="features">The list of features that we are using</param>
        /// <param name="centre">The centre of the image</param>
        /// <returns>The resultant calibration parameters</returns>
        public static CalibrationResult Extract(Matrix<double> L, bool positiveTy, List<FeaturePoint> features, MCvPoint2D64f centre) 
        {
            var result = new CalibrationResult();
            result.Centre = centre;
            result.Translation = new Matrix<double>(3, 1);
            result.Translation[1, 0] = GetTy(L, positiveTy);
            result.Sx = GetSx(L, result.Translation[1, 0]);
            result.Translation[0, 0] = GetTx(L, result.Translation[1, 0], result.Sx);
            result.Rotation = GetRotation(L, result.Translation[1, 0], result.Sx);
            return result;
        }

        #endregion

        #region Basic extraction Methods

        /// <summary>
        /// Find the value for sx
        /// </summary>
        /// <param name="L">The current L matrix</param>
        /// <param name="ty">The value associated with ty</param>
        /// <returns>The calculated result of sx</returns>
        private static double GetSx(Matrix<double> L, double ty)
        {
            var sum = L[0, 0] * L[0, 0] + L[1, 0] * L[1, 0] + L[2, 0] * L[2, 0];
            return Math.Abs(ty) * Math.Sqrt(sum);
        }

        /// <summary>
        /// Find the value for Tx
        /// </summary>
        /// <param name="L">The current L matrix</param>
        /// <param name="ty">The value associated with ty</param>
        /// <returns>The calculated result of Tx</returns>
        private static double GetTx(Matrix<double> L, double ty, double sx)
        {
            return L[3, 0] * ty / sx;
        }

        /// <summary>
        /// Find the value for Ty
        /// </summary>
        /// <param name="L">The current L matrix</param>
        /// <param name="positiveTy">Indicated whether ty should be positive or negative</param>
        /// <returns>The calculated result of ty</returns>
        private static double GetTy(Matrix<double> L, bool positiveTy)
        {
            var sum = L[4, 0] * L[4, 0] + L[5, 0] * L[5, 0] + L[6, 0] * L[6, 0];
            var ty = Math.Abs(1.0 / Math.Sqrt(sum));
            return positiveTy ? ty : -ty;
        }

        #endregion

        #region Rotation Extraction Method

        /// <summary>
        /// Logic to perform the extraction of rotation information
        /// </summary>
        /// <param name="L">The L Matrix that we are getting values from</param>
        /// <param name="ty">The ty value</param>
        /// <param name="sx">The sx value</param>
        /// <returns>The rotation that we have extracted</returns>
        private static Matrix<double> GetRotation(Matrix<double> L, double ty, double sx)
        {
            var row1 = new MCvPoint3D64f(L[0, 0] * ty / sx, L[1, 0] * ty / sx, L[2, 0] * ty / sx);
            var row2 = new MCvPoint3D64f(L[4, 0] * ty, L[5, 0] * ty, L[6, 0] * ty);
            var row3 = row1.CrossProduct(row2); row3 = Normalize(row3);
            return MakeMatrix(row1, row2, row3);
        }

        /// <summary>
        /// Normalize the associated value
        /// </summary>
        /// <param name="value">The value that we are normalizing</param>
        /// <returns>The normalized value</returns>
        private static MCvPoint3D64f Normalize(MCvPoint3D64f value)
        {
            var sum = value.X * value.X + value.Y * value.Y + value.Z * value.Z;
            var magnitude = Math.Sqrt(sum);
            return value * (1.0 / magnitude);
        }

        /// <summary>
        /// Make a matrix from the 3 given rows
        /// </summary>
        /// <param name="row1">The first row that we have been given</param>
        /// <param name="row2">The second row that we have been given</param>
        /// <param name="row3">The last row that we have been given</param>
        /// <returns>The resultant matrix</returns>
        private static Matrix<double> MakeMatrix(MCvPoint3D64f row1, MCvPoint3D64f row2, MCvPoint3D64f row3)
        {
            var rotation = new Matrix<double>(3, 3);
            rotation[0, 0] = row1.X;
            rotation[0, 1] = row1.Y;
            rotation[0, 2] = row1.Z;
            rotation[1, 0] = row2.X;
            rotation[1, 1] = row2.Y;
            rotation[1, 2] = row2.Z;
            rotation[2, 0] = row3.X;
            rotation[2, 1] = row3.Y;
            rotation[2, 2] = row3.Z;
            return rotation;           
        }

        #endregion
    }
}
