﻿using TsaiEngine.Models;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Collections.Generic;
using System;
using TsaiEngine.Utilities;

namespace TsaiEngine.Tsai
{
    /// <summary>
    /// Defines the logic to perform calibration
    /// </summary>
    public class LinearCalibrator
    {
        #region Calibration Entry Point

        /// <summary>
        /// Defines the actual calibration logic
        /// </summary>
        /// <param name="problem">The calibraiton problem that are are finding a solution to</param>
        /// <param name="parameters">The parameters associated with the problem</param>
        public static void Calibrate(CalibrationProblem problem)
        {
            // Undistort the feature points
            var features = UndistortPoints(problem.CalibrationResult, problem.Features);

            // Initialize the solver
            var solver = new LSolver(features, problem.CalibrationResult.Centre);

            // Perform the calibration
            var L = solver.CalculateLMatrix();
            CalibrationResult calibration = GetCalibrationResult(problem.CalibrationResult.Centre, features, L, true);
            if (calibration.Focal < 0) calibration = GetCalibrationResult(problem.CalibrationResult.Centre, features, L, false);

            // Copy the distortion coefficients into the new result
            //calibration.K1 = problem.CalibrationResult.K1;
            //calibration.K2 = problem.CalibrationResult.K2;
            //calibration.P1 = problem.CalibrationResult.P1;
            //calibration.P2 = problem.CalibrationResult.P2;

            // Set the result of the calibration = the curernt calibration result
            problem.CalibrationResult = calibration; 

            // Raise an exception of a proper calibration (with a positive F) could not be found!
            if (calibration.Focal < 0) throw new ArgumentException("Unable to calibrate, check your feature points are in the correct orientation!");
        }

        #endregion

        #region Find the calibration Result

        /// <summary>
        /// Get the calibration result from the data
        /// </summary>
        /// <param name="centre">The centre that we are dealing with</param>
        /// <param name="features">The associated features</param>
        /// <param name="L">The L matrix</param>
        /// <param name="positiveTy">Indicates if we should assume Ty to be positive result</param>
        /// <returns>The calibration result</returns>
        private static CalibrationResult GetCalibrationResult(MCvPoint2D64f centre, List<FeaturePoint> features, Matrix<double> L, bool positiveTy)
        {
            var calibration = LExtractor.Extract(L, positiveTy, features, centre);
            var finder = new FocalFinder(features, centre, calibration.Rotation, calibration.Translation[1, 0]);
            var fandTz = finder.CalculateResult();
            calibration.Focal = fandTz[0, 0]; calibration.Translation[2, 0] = fandTz[1, 0];
            return calibration;
        }

        #endregion

        #region Undistort Logic for the points

        /// <summary>
        /// Undistort the list of feature points that we are dealing with
        /// </summary>
        /// <param name="calibrationResult">The calibration result holding the distortion parameters</param>
        /// <param name="features">The features that we are getting</param>
        /// <returns>The feature point that we are getting together</returns>
        private static List<FeaturePoint> UndistortPoints(CalibrationResult calibrationResult, List<FeaturePoint> features)
        {
            var cameraMatrix = BuildCameraMatrix(calibrationResult);
            var distortionMatrix = BuildDistortionMatrix(calibrationResult);
            var distorter = new ZhangDistorter(cameraMatrix);

            var result = new List<FeaturePoint>();
            foreach (var feature in features) 
            {
                var imagePoint = feature.GetImageCoordinate();
                var newImagePoint = distorter.UnDistort(distortionMatrix, imagePoint);
                result.Add(new FeaturePoint(feature.GetSceneCoordinate(), newImagePoint));
            }

            return result;
        }

        /// <summary>
        /// Build the associated camera matrix
        /// </summary>
        /// <param name="calibrationResult">The calibration result that we are building from</param>
        /// <returns>The resultant camera matrix</returns>
        private static Matrix<double> BuildCameraMatrix(CalibrationResult calibrationResult)
        {
            var cameraMatrix = new Matrix<double>(3, 3); cameraMatrix.SetIdentity();
            var focal = (calibrationResult.Centre.X * 2 + calibrationResult.Centre.Y * 2);

            cameraMatrix.Data[0, 0] = focal;
            cameraMatrix.Data[0, 2] = calibrationResult.Centre.X + calibrationResult.CentreOffset.X;
            cameraMatrix.Data[1, 1] = focal;
            cameraMatrix.Data[1, 2] = calibrationResult.Centre.Y + calibrationResult.CentreOffset.Y;

            return cameraMatrix;
        }

        /// <summary>
        /// Build the associated distortion matrix
        /// </summary>
        /// <param name="calibrationResult">The calibration result that we are building from</param>
        /// <returns>The resultant distortion matrix</returns>
        private static Matrix<double> BuildDistortionMatrix(CalibrationResult calibrationResult)
        {
            var K1 = calibrationResult.K1;
            var K2 = calibrationResult.K2;
            var P1 = calibrationResult.P1;
            var P2 = calibrationResult.P2;

            return new Matrix<double>(new double[] { K1, K2, P1, P2 });
        }

        #endregion
    }
}
