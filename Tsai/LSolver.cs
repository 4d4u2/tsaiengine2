﻿using Emgu.CV;
using Emgu.CV.Structure;
using TsaiEngine.Models;
using System.Collections.Generic;
using System.Linq;

namespace TsaiEngine.Tsai
{
    /// <summary>
    /// A solver for finding the LMatrix in Tsai calibration
    /// </summary>
    public class LSolver
    {
        #region Private Variables

        private Matrix<double> _x;
        private Matrix<double> _m;

        #endregion

        #region Constructors

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="features">The feature points that we are processing</param>
        /// <param name="centre">The centre of the image</param>
        public LSolver(List<FeaturePoint> features, MCvPoint2D64f centre) 
        {
            _x = AssembleXMatrix(features, centre);
            _m = AssembleMMatrix(features, centre);
        }

        #endregion

        #region Getter methods

        /// <summary>
        /// Retrieve the current X matrix 
        /// </summary>
        /// <returns>The x matrix that we have calculated</returns>
        public Matrix<double> GetXMatrix() 
        {
            return _x;
        }

        /// <summary>
        /// Retrieve the current M matrix
        /// </summary>
        /// <returns>The m matrix that we have calculated</returns>
        public Matrix<double> GetMMatrix() 
        {
            return _m;
        }

        #endregion

        #region Logic to calculate the LMatrix

        /// <summary>
        /// Defines the logic to calculate the L Matrix
        /// </summary>
        /// <returns>The resultant L Matrix that we have calculated</returns>
        public Matrix<double> CalculateLMatrix() 
        {
            var result = new Matrix<double>(7, 1);
            CvInvoke.Solve(_m, _x, result, Emgu.CV.CvEnum.DecompMethod.QR);
            return result;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Construct the X matrix given a set of features and the image centre
        /// </summary>
        /// <param name="features">The list of features that we are constructing from</param>
        /// <param name="centre">The centre of the image that we are using</param>
        /// <returns>The resultant matrix after the construction process</returns>
        private Matrix<double> AssembleXMatrix(List<FeaturePoint> features, MCvPoint2D64f centre)
        {
            var result = new Matrix<double>(features.Count, 1); int counter = 0;
            for (int i = 0; i < features.Count; i++) 
            {
                result[counter++, 0] = features[i].GetImageCoordinate().X - centre.X;            
            }
            return result;
        }

        /// <summary>
        /// Construct the M matrix given a set of features and the image centre
        /// </summary>
        /// <param name="features">The list of features that we are cosntructing from</param>
        /// <param name="centre">The centre of the image that we are using</param>
        /// <returns>The resultant matrix after the construction process</returns>
        private Matrix<double> AssembleMMatrix(List<FeaturePoint> features, MCvPoint2D64f centre)
        {
            var result = new Matrix<double>(features.Count, 7); int counter = 0;
            for (int i = 0; i < features.Count; i++)
            {
                result[counter, 0] = (features[i].GetImageCoordinate().Y - centre.Y) * features[i].GetSceneCoordinate().X;
                result[counter, 1] = (features[i].GetImageCoordinate().Y - centre.Y) * features[i].GetSceneCoordinate().Y;
                result[counter, 2] = (features[i].GetImageCoordinate().Y - centre.Y) * features[i].GetSceneCoordinate().Z;
                result[counter, 3] = (features[i].GetImageCoordinate().Y - centre.Y);
                result[counter, 4] = -(features[i].GetImageCoordinate().X - centre.X) * features[i].GetSceneCoordinate().X;
                result[counter, 5] = -(features[i].GetImageCoordinate().X - centre.X) * features[i].GetSceneCoordinate().Y;
                result[counter, 6] = -(features[i].GetImageCoordinate().X - centre.X) * features[i].GetSceneCoordinate().Z;
                counter++;
            }
            return result;
        }

        #endregion
    }
}
