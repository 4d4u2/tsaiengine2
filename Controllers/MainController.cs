﻿using Emgu.CV.Structure;
using ProjectionTest.Utilities;
using System;
using TsaiEngine.Models;
using TsaiEngine.Tsai;
using TsaiEngine.Utilities;
using TsaiEngine.Utility;

namespace TsaiEngine.Controllers
{
    /// <summary>
    /// Main Controller that we are dealing with
    /// </summary>
    public class MainController
    {
        #region Private Variables

        private Arguments _arguments;
        private CalibrationProblem _problem1;
        private CalibrationProblem _problem2;

        #endregion

        #region Constructors

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="arguments">The arguments associated with the application</param>
        public MainController(Arguments arguments) 
        {
            _arguments = arguments;
            
            _problem1 = new CalibrationProblem(); _problem2 = new CalibrationProblem();
            
            _problem1.Features = PointLoader.Load(arguments.PointSetPath1);
            _problem2.Features = PointLoader.Load(arguments.PointSetPath2);

            DistortionFiller.Fill(_arguments.DistortionPath1, _arguments.Parameters, _problem1.CalibrationResult);
            DistortionFiller.Fill(_arguments.DistortionPath2, _arguments.Parameters, _problem2.CalibrationResult);
        }

        #endregion

        #region Main Execution Logic

        /// <summary>
        /// Main Entry Point
        /// </summary>
        public void Run() 
        {
            Calibrate(_arguments.OutputPath1, _problem1); Calibrate(_arguments.OutputPath2, _problem2);
        }

        #endregion

        #region Tsai Calibration Logic

        /// <summary>
        /// Perform calibration on the given calibration problem
        /// </summary>
        /// <param name="path">The path to the associated calibration problem/param>
        /// <param name="problem">The problem that we are calibrating</param>
        private void Calibrate(string path, CalibrationProblem problem) 
        {
            LinearCalibrator.Calibrate(problem);
            new Refiner().OptimizeDistortion(problem);

            var errors = new double[problem.Features.Count];
            var totalError = ErrorFinder.GetError(problem, errors);

            Console.WriteLine();
            Console.WriteLine("Focal: " + problem.CalibrationResult.Focal);
            Console.WriteLine("Sx: " + problem.CalibrationResult.Sx);
            Console.WriteLine("CX: " + (problem.CalibrationResult.Centre.X + problem.CalibrationResult.CentreOffset.X));
            Console.WriteLine("CY: " + (problem.CalibrationResult.Centre.Y + problem.CalibrationResult.CentreOffset.Y));
            Console.WriteLine("K1: " + problem.CalibrationResult.K1);
            Console.WriteLine("K2: " + problem.CalibrationResult.K2);
            Console.WriteLine("P1: " + problem.CalibrationResult.P1);
            Console.WriteLine("P2: " + problem.CalibrationResult.P2);

            Console.WriteLine(); Console.WriteLine("Rotation: ");
            Console.WriteLine(problem.CalibrationResult.Rotation.Data[0, 0] + " " + problem.CalibrationResult.Rotation.Data[0, 1] + " " + problem.CalibrationResult.Rotation.Data[0, 2]);
            Console.WriteLine(problem.CalibrationResult.Rotation.Data[1, 0] + " " + problem.CalibrationResult.Rotation.Data[1, 1] + " " + problem.CalibrationResult.Rotation.Data[1, 2]);
            Console.WriteLine(problem.CalibrationResult.Rotation.Data[2, 0] + " " + problem.CalibrationResult.Rotation.Data[2, 1] + " " + problem.CalibrationResult.Rotation.Data[2, 2]);

            Console.WriteLine();
            Console.WriteLine("TX: " + problem.CalibrationResult.Translation.Data[0, 0]);
            Console.WriteLine("TY: " + problem.CalibrationResult.Translation.Data[1, 0]);
            Console.WriteLine("TZ: " + problem.CalibrationResult.Translation.Data[2, 0]);
            Console.WriteLine("Average Error: " + (totalError / problem.Features.Count));

            CalibrationSaver.Save(path, problem.CalibrationResult);
        }

        #endregion
    }
}
