﻿using System;
using TsaiEngine.Controllers;
using TsaiEngine.Models;

namespace TsaiEngine
{
    /// <summary>
    /// Main entry point application
    /// </summary>
    public class Program
    {
        #region Entry Point Method

        /// <summary>
        /// Main entry point of the application
        /// </summary>
        /// <param name="arguments">The arguments associated with the application</param>
        public static void Main(string[] parameters)
        {
            try
            {
                var arguments = new Arguments();
                new MainController(arguments).Run(); 
                Pause();
            }
            catch (Exception exception) 
            {
                Console.Error.WriteLine("ERROR: " + exception.Message); Pause();
            }
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Defines the logic to pause the screen
        /// </summary>
        private static void Pause()
        {
            Console.WriteLine(); Console.WriteLine("Press ENTER to continue...");
            Console.ReadLine();
        }

        #endregion
    }
}
