﻿using TsaiEngine.Utilities;
using System;
using System.IO;

namespace TsaiEngine.Models
{
    /// <summary>
    /// Defines the arguments associated with the application
    /// </summary>
    public class Arguments
    {
        #region Private Static Constants

        private static readonly string BASE_PATH = @"D:\Trevor\Research\Calibration\Stereo\Process";

        #endregion

        #region Main Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public Arguments() 
        {
            Parameters = ParameterLoader.Load(Path.Combine(BASE_PATH, "Config.xml"));

            PointSetPath1 = BuildPointsPath(Parameters.ActiveSet, 0);
            PointSetPath2 = BuildPointsPath(Parameters.ActiveSet, 1);
            DistortionPath1 = BuildDistortionPath(Parameters.ActiveSet, 0);
            DistortionPath2 = BuildDistortionPath(Parameters.ActiveSet, 1);
            OutputPath1 = BuildOutputPath(Parameters.ActiveSet, 0);
            OutputPath2 = BuildOutputPath(Parameters.ActiveSet, 1);
        }

        #endregion

        #region Path Builders

        /// <summary>
        /// Build the path associated with the points
        /// </summary>
        /// <param name="activeSet">The active set that we are calculating</param>
        /// <param name="cameraNumber">The number of the camera that we are using</param>
        /// <returns>The path of the point file</returns>
        private string BuildPointsPath(int activeSet, int cameraNumber)
        {
            var folder = Path.Combine(BASE_PATH, "FinalPoints");
            var fileName = string.Format("Set_{0:0000}_Points_{1}.xml", activeSet, cameraNumber);
            return Path.Combine(folder, fileName);
        }

        /// <summary>
        /// Build the path to where to save the distortion result
        /// </summary>
        /// <param name="activeSet">The current active set that we are working with</param>
        /// <param name="cameraNumber">The camera that we are processing</param>
        /// <returns>The resultant path to the distortion file</returns>
        private string BuildDistortionPath(int activeSet, int cameraNumber)
        {
            var folder = Path.Combine(BASE_PATH, "Attributes");
            var fileName = string.Format("Set_{0:0000}_Distortion_{1}.xml", activeSet, cameraNumber);
            return Path.Combine(folder, fileName);
        }

        /// <summary>
        /// Construct the output path for saving the path
        /// </summary>
        /// <param name="activeSet">The current active set that we are working with</param>
        /// <param name="cameraNumber">The camera that we are processing</param>
        /// <returns>The resultant path that we are saving</returns>
        private string BuildOutputPath(int activeSet, int cameraNumber)
        {
            var folder = Path.Combine(BASE_PATH, "Calibration");
            var fileName = string.Format("Set_{0:0000}_Calibration_{1}.xml", activeSet, cameraNumber);
            return Path.Combine(folder, fileName);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The path to the first point set
        /// </summary>
        public string PointSetPath1 { get; private set; }

        /// <summary>
        /// The path to the second point set
        /// </summary>
        public string PointSetPath2 { get; private set; }

        /// <summary>
        /// The path to write the first distoriton file
        /// </summary>
        public string DistortionPath1 { get; private set; }

        /// <summary>
        /// The path to write the second distortion file
        /// </summary>
        public string DistortionPath2 { get; private set; }

        /// <summary>
        /// Defines the parameters associated with the system
        /// </summary>
        public Parameters Parameters { get; private set; }

        /// <summary>
        /// The path to the output of the left frame points
        /// </summary>
        public string OutputPath1 { get; private set; }

        /// <summary>
        /// The path to the output of the right frame points
        /// </summary>
        public string OutputPath2 { get; private set; }        
        
        #endregion
    }
}
