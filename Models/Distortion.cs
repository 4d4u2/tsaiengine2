﻿namespace TsaiEngine.Models
{
    /// <summary>
    /// Defines the distortion within the system
    /// </summary>
    public class Distortion
    {
        #region Public Properties

        /// <summary>
        /// The first radial distortion coefficient
        /// </summary>
        public double K1 { get; set; }

        /// <summary>
        /// The second radial distortion coefficient
        /// </summary>
        public double K2 { get; set; }

        /// <summary>
        /// The first tangential distortion coefficient
        /// </summary>
        public double P1 { get; set; }

        /// <summary>
        /// The second tangential distortion coefficient
        /// </summary>
        public double P2 { get; set; }

        /// <summary>
        /// The first component of the optical center
        /// </summary>
        public double CX { get; set; }

        /// <summary>
        /// the second component of the optical center
        /// </summary>
        public double CY { get; set; }

        #endregion
    }
}