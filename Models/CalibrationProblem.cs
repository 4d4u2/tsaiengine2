﻿using Emgu.CV.Structure;
using System.Collections.Generic;
using System.Drawing;

namespace TsaiEngine.Models
{
    /// <summary>
    /// Definition of the calibration problem that we are working with0
    /// </summary>
    public class CalibrationProblem
    {
        #region Constructors

        /// <summary>
        /// Main Constructor
        /// </summary>
        public CalibrationProblem() 
        {
            CalibrationResult = new CalibrationResult();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The feature points in the problem space
        /// </summary>
        public List<FeaturePoint> Features { get; set; }
        
        /// <summary>
        /// The results after calibration
        /// </summary>
        public CalibrationResult CalibrationResult { get; set; }

        #endregion
    }
}
