﻿using Emgu.CV;
using Emgu.CV.Structure;

namespace TsaiEngine.Models
{
    /// <summary>
    /// Defines the results of the calibration process
    /// </summary>
    public class CalibrationResult
    {
        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public CalibrationResult()
        {
            // Extra Initialization can go here
        }

        /// <summary>
        /// Clone constructor
        /// </summary>
        /// <param name="result">The result that we are cloning</param>
        public CalibrationResult(CalibrationResult result)
        {
            Focal = result.Focal;
            Sx = result.Sx;
            Centre = result.Centre;
            Rotation = result.Rotation.Clone();
            Translation = result.Translation.Clone();
            K1 = result.K1;
            K2 = result.K2;
            P1 = result.P1;
            P2 = result.P2;
            CentreOffset = result.CentreOffset;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The focal length
        /// </summary>
        public double Focal { get; set; }

        /// <summary>
        /// The scaling factor in the x-direction
        /// </summary>
        public double Sx { get; set; }

        /// <summary>
        /// Optic Centre
        /// </summary>
        public MCvPoint2D64f Centre { get; set; }

        /// <summary>
        /// Extrinsic rotation of the camera
        /// </summary>
        public Matrix<double> Rotation { get; set; }

        /// <summary>
        /// Extrinsic translation of the camera
        /// </summary>
        public Matrix<double> Translation { get; set; }

        /// <summary>
        /// The first radial distortion value
        /// </summary>
        public double K1 { get; set; }

        /// <summary>
        /// The second radial distortion value
        /// </summary>
        public double K2 { get; set; }

        /// <summary>
        /// The first tangential parameter
        /// </summary>
        public double P1 { get; set; }

        /// <summary>
        /// The second tangential parameter
        /// </summary>
        public double P2 { get; set; }

        /// <summary>
        /// Defines the associated offset in the centre
        /// </summary>
        public MCvPoint2D64f CentreOffset { get; set; }

        #endregion
    }
}
