﻿using Emgu.CV.Structure;

namespace TsaiEngine.Models
{
    /// <summary>
    /// Defines a basic feature point within the system
    /// </summary>
    public class FeaturePoint
    {
        #region Private Variables

        private MCvPoint2D64f _imageCoordinate;
        private MCvPoint3D64f _sceneCoordinate;

        #endregion

        #region Main Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="sceneCoordinate">The coordinate in the given scene</param>
        /// <param name="imageCoordinate">The coordinate in the given image</param>
        public FeaturePoint(MCvPoint3D64f sceneCoordinate, MCvPoint2D64f imageCoordinate) 
        {
            _sceneCoordinate = sceneCoordinate;
            _imageCoordinate = imageCoordinate;
        }

        #endregion

        #region Getter Methods

        /// <summary>
        /// Retrieve the coordinate of the feature on the image
        /// </summary>
        /// <returns>The location of the feature on the image</returns>
        public MCvPoint2D64f GetImageCoordinate() 
        {
            return _imageCoordinate;
        }

        /// <summary>
        /// Retrieve the location of the feature within the scene
        /// </summary>
        /// <returns>The location of the feature within the scene</returns>
        public MCvPoint3D64f GetSceneCoordinate() 
        {
            return _sceneCoordinate;
        }

        #endregion

        #region Setter Methods

        /// <summary>
        /// Set the location jof the scene coordinates
        /// </summary>
        /// <param name="coordinate">The new coordinate value taht we are setting</param>
        public void SetSceneCoordinate(MCvPoint3D64f coordinate)
        {
            _sceneCoordinate = coordinate;
        }

        #endregion
    }
}
