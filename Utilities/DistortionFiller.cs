﻿using Emgu.CV;
using Emgu.CV.Structure;
using TsaiEngine.Models;

namespace TsaiEngine.Utility
{
    /// <summary>
    /// Defines the functionality to load the distortion from disk
    /// </summary>
    public class DistortionFiller
    {
        #region Loading Functionality

        /// <summary>
        /// Load the associated distortion from disk
        /// </summary>
        /// <param name="fileName">The name of the file that we are loading</param>
        /// <param name="parameters">The parameters that we are filling</param>
        /// <param name="details">The details that we are populating</param>
        public static void Fill(string fileName, Parameters parameters, CalibrationResult details) 
        {
            using (var reader = new FileStorage(fileName, FileStorage.Mode.Read | FileStorage.Mode.FormatXml)) 
            {
                details.K1 = reader.GetNode("K1").ReadDouble();
                details.K2 = reader.GetNode("K2").ReadDouble();
                details.P1 = reader.GetNode("P1").ReadDouble();
                details.P2 = reader.GetNode("P2").ReadDouble();
                details.Centre = new MCvPoint2D64f(parameters.Width / 2.0, parameters.Height / 2.0);
                
                // Fill in the offsets
                var CX = reader.GetNode("CX").ReadDouble();
                var CY = reader.GetNode("CY").ReadDouble();
                var offsetX = CX - details.Centre.X;
                var offsetY = CY - details.Centre.Y;
                details.CentreOffset = new MCvPoint2D64f(offsetX, offsetY);
            }
        }

        #endregion
    }
} 