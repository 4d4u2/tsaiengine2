﻿using Emgu.CV;
using TsaiEngine.Models;

namespace TsaiEngine.Utilities
{
    /// <summary>
    /// Defines the logic for saving the calibration result to disk
    /// </summary>
    public class CalibrationSaver
    {
        #region Saving Result

        /// <summary>
        /// Save the calibration result to disk
        /// </summary>
        /// <param name="path">The path that we are saving</param>
        /// <param name="calibration">The calibration that we are saving</param>
        public static void Save(string path, CalibrationResult calibration) 
        {
            var intrinsicMatrix = BuildIntrinsics(calibration);
            var extrinsicMatrix = BuildExtrinsics(calibration);
            var distortionMatrix = BuildDistortionMatrix(calibration);
            SaveMatrices(path, intrinsicMatrix, extrinsicMatrix, distortionMatrix);
        }

        #endregion

        #region Build the intrinsic Matrix

        /// <summary>
        /// Build the associated intrinsics matrix
        /// </summary>
        /// <param name="calibration">The calibration result</param>
        /// <returns>The intrinsics that we are dealing with</returns>
        private static Matrix<double> BuildIntrinsics(CalibrationResult calibration)
        {
            var result = new Matrix<double>(3, 3); result.SetIdentity();
            result.Data[0, 0] = calibration.Sx * calibration.Focal;
            result.Data[0, 2] = calibration.Centre.X + calibration.CentreOffset.X;
            result.Data[1, 1] = calibration.Focal;
            result.Data[1, 2] = calibration.Centre.Y + calibration.CentreOffset.Y;
            return result;
        }

        #endregion

        #region Build the Extrinsic Matrix

        /// <summary>
        /// Build the extrinsics that we are dealing with
        /// </summary>
        /// <param name="calibration">The calibration result</param>
        /// <returns>The extrinsics that we are dealing with</returns>
        private static Matrix<double> BuildExtrinsics(CalibrationResult calibration)
        {
            var result = new Matrix<double>(4, 4); result.SetIdentity();

            for (int row = 0; row < 3; row++) 
            {
                for (int column = 0; column < 3; column++) 
                {
                    result.Data[row, column] = calibration.Rotation.Data[row, column];
                }
            }

            result.Data[0, 3] = calibration.Translation.Data[0, 0];
            result.Data[1, 3] = calibration.Translation.Data[1, 0];
            result.Data[2, 3] = calibration.Translation.Data[2, 0];

            return result;
        }

        #endregion

        #region Build a matrix holding the various distortion properties

        /// <summary>
        /// Build a distortion matrix from the calibration result
        /// </summary>
        /// <param name="calibration">The calibration parameters</param>
        /// <returns>The distortion matrix that we have extracted</returns>
        private static Matrix<double> BuildDistortionMatrix(CalibrationResult calibration)
        {
            var result = new Matrix<double>(4, 1);
            result.Data[0, 0] = calibration.K1;
            result.Data[1, 0] = calibration.K2;
            result.Data[2, 0] = calibration.P1;
            result.Data[3, 0] = calibration.P2;
            return result;
        }

        #endregion

        #region Defines the Save Logic for the various Matrices

        /// <summary>
        /// Save the matrices that we are dealing with
        /// </summary>
        /// <param name="path">The path that we are saving</param>
        /// <param name="intrinsicMatrix">The intrinsic matrix being saved</param>
        /// <param name="extrinsicMatrix">The extrinsic matrix being saved</param>
        /// <param name="distortionMatrix">The distoriton matrix that is being saved</param>
        private static void SaveMatrices(string path, Matrix<double> intrinsicMatrix, Matrix<double> extrinsicMatrix, Matrix<double> distortionMatrix)
        {
            using (var writer = new FileStorage(path, FileStorage.Mode.Write | FileStorage.Mode.FormatXml)) 
            {
                writer.Write(intrinsicMatrix.Mat, "Intrinsics");
                writer.Write(extrinsicMatrix.Mat, "Extrinsics");
                writer.Write(distortionMatrix.Mat, "Distortion");
            }
        }

        #endregion
    }
}