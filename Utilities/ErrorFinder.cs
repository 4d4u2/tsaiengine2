﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using TsaiEngine.Models;
using TsaiEngine.Utilities;

namespace ProjectionTest.Utilities
{
    /// <summary>
    /// Defines the logic to define a set of repo errors for a given set of points
    /// </summary>
    public class ErrorFinder
    {
        #region Functionality Entry Point

        /// <summary>
        /// Determine the error associated with a given set of points
        /// </summary>
        /// <param name="problem">The calibration problem that we are trying to solve</param>
        /// <param name="errors">The individual errors</param>
        /// <returns>The list of points that we have returned</returns>
        public static double GetError(CalibrationProblem problem, double[] errors) 
        {
            var total = 0.0;

            var cameraMatrix = BuildCameraMatrix(problem);
            var pose = BuildPose(problem);
            var distortion = BuildDistortion(problem);

            int counter = 0;
            foreach (var feature in problem.Features) 
            {
                var tPoint = Transform(pose, feature.GetSceneCoordinate());
                var pPoint = Project(cameraMatrix, tPoint);
                var dPoint = Distort(cameraMatrix, distortion, pPoint);
                var residual = FindResidual(feature.GetImageCoordinate(), dPoint);
                total += residual; errors[counter++] = residual; 
            }

            return total;
        }

        #endregion

        #region Matrix Constructors

        /// <summary>
        /// Build the associated camera matrix
        /// </summary>
        /// <param name="problem">The problem that we are trying to solve</param>
        /// <returns>The problem that we are solving</returns>
        private static Matrix<double> BuildCameraMatrix(CalibrationProblem problem)
        {
            var result = new Matrix<double>(3, 3); result.SetIdentity();

            result.Data[0, 0] = problem.CalibrationResult.Focal * problem.CalibrationResult.Sx;
            result.Data[0, 2] = problem.CalibrationResult.Centre.X + problem.CalibrationResult.CentreOffset.X;
            result.Data[1, 1] = problem.CalibrationResult.Focal;
            result.Data[1, 2] = problem.CalibrationResult.Centre.Y + problem.CalibrationResult.CentreOffset.Y;

            return result;
        }

        /// <summary>
        /// Build the pose matrix from the problem
        /// </summary>
        /// <param name="problem">The problem that we are extracting the pose from</param>
        /// <returns>The matrix holding the pose</returns>
        private static Matrix<double> BuildPose(CalibrationProblem problem)
        {
            var result = new Matrix<double>(4, 4); result.SetIdentity();

            for (int row = 0; row < 3; row++) 
            {
                for (int column = 0; column < 3; column++) 
                {
                    result.Data[row, column] = problem.CalibrationResult.Rotation[row, column];
                }
            }

            result.Data[0, 3] = problem.CalibrationResult.Translation.Data[0, 0];
            result.Data[1, 3] = problem.CalibrationResult.Translation.Data[1, 0];
            result.Data[2, 3] = problem.CalibrationResult.Translation.Data[2, 0];

            return result;
        }

        /// <summary>
        /// Build the associated distortion matrix
        /// </summary>
        /// <param name="problem">The problem that we are getting the distortion matrix for</param>
        /// <returns>The resultant distortion matrix</returns>
        private static Matrix<double> BuildDistortion(CalibrationProblem problem)
        {
            var result = new Matrix<double>(4, 1);

            result.Data[0, 0] = problem.CalibrationResult.K1;
            result.Data[1, 0] = problem.CalibrationResult.K2;
            result.Data[2, 0] = problem.CalibrationResult.P1;
            result.Data[3, 0] = problem.CalibrationResult.P2;

            return result;
        }

        #endregion

        #region Image Transformation Helpers

        /// <summary>
        /// Defines the logic to transform the given point associated by the pose
        /// </summary>
        /// <param name="pose">The pose that we are transforming</param>
        /// <param name="point">The point that is being transformed</param>
        /// <returns>The resultant point</returns>
        private static MCvPoint3D64f Transform(Matrix<double> pose, Emgu.CV.Structure.MCvPoint3D64f point)
        {
            var X = point.X * pose.Data[0, 0] + point.Y * pose.Data[0, 1] + point.Z * pose.Data[0, 2] + pose.Data[0, 3];
            var Y = point.X * pose.Data[1, 0] + point.Y * pose.Data[1, 1] + point.Z * pose.Data[1, 2] + pose.Data[1, 3];
            var Z = point.X * pose.Data[2, 0] + point.Y * pose.Data[2, 1] + point.Z * pose.Data[2, 2] + pose.Data[2, 3];
            return new MCvPoint3D64f(X, Y, Z);
        }

        /// <summary>
        /// Project a 3-D point into 2-D spac
        /// </summary>
        /// <param name="cameraMatrix">The camera matrix that we are using for the projection</param>
        /// <param name="point">The point that we are projecting</param>
        /// <returns>The point that we have projected</returns>
        private static MCvPoint2D64f Project(Matrix<double> cameraMatrix, MCvPoint3D64f point)
        {
            var u = (cameraMatrix.Data[0, 0] * point.X / point.Z) + cameraMatrix.Data[0, 2];
            var v = (cameraMatrix.Data[1, 1] * point.Y / point.Z) + cameraMatrix.Data[1, 2];
            return new MCvPoint2D64f(u, v);
        }

        /// <summary>
        /// Add distortion to the associated point
        /// </summary>
        /// <param name="cameraMatrix">The camera matrix that we are using</param>
        /// <param name="distortion">The set of distortion coefficients that we are applying to the given point</param>
        /// <param name="point">The point that we are applying distortion to</param>
        /// <returns>The resultant distorted point</returns>
        private static MCvPoint2D64f Distort(Matrix<double> cameraMatrix, Matrix<double> distortion, MCvPoint2D64f point)
        {
            var distorter = new ZhangDistorter(cameraMatrix);
            return distorter.Distort(distortion, point);
        }

        /// <summary>
        /// Calculate the difference between the given set of two points
        /// </summary>
        /// <param name="point1">The first point that we are getting values for</param>
        /// <param name="point2">The second point that we are getting values for</param>
        /// <returns>The residual between points that we are getting values for</returns>
        private static double FindResidual(MCvPoint2D64f point1, MCvPoint2D64f point2)
        {
            var xDiff = point1.X - point2.X;
            var yDiff = point1.Y - point2.Y;
            return Math.Sqrt(xDiff * xDiff + yDiff * yDiff);
        }

        #endregion
    }
}